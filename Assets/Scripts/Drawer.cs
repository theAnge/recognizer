﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Drawer : MonoBehaviour {
    //Transform draw;
    Vector3 lastPosition;
    public List<Color> Colors;
    bool drawing;
    public Pull pull;
    Color drawColor;
    System.Random rand = new System.Random();
    // Use this for initialization
    void OnEnable() {
        //draw = drawsPoints[Random.Range(0, drawsPoints.Length - 1)];
        drawColor = Colors[rand.Next(Colors.Count-1)];
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            lastPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            lastPosition.z = 0;
            StartCoroutine(Drow());
        }
        
	}
    IEnumerator Drow()
    {
        drawing = true;
        while (true)
        {
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            worldPosition.z = 0;
            var wayPoints= lastPosition.WayPoints(worldPosition,10);
            foreach (var position in wayPoints)
            {
                DrawPoint dp = pull.Pulling();
                //yield return new WaitForSeconds(0);
                dp.transform.position = position;
                dp.ChangeColor(drawColor);
            }
            if (Input.GetMouseButtonUp(0))
            {
                drawing = false;
                // draw = drawsPoints[Random.Range(0, drawsPoints.Length - 1)];
                drawColor = Colors[rand.Next(Colors.Count - 1)];
                yield break;
            }
            lastPosition = worldPosition;
            yield return new WaitForSeconds(0);

        }
    }

}

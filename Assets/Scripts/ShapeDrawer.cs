﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShapeDrawer : MonoBehaviour {
    public List<Shape> shapes;
    Transform DrawingPoint;
    public Transform[] DrawingPoints;
    public Pull pull;
    public List<Color> colors;
    Color color;
    // Use this for initialization
    void Start () {
        
        ChoiceRandomDrawingPoint();
        
        
	}
    public void Drow(Shape shape)
    {
        List<Vector3> frame = shape.frame.FixFrame();
        List<Vector3> waytoDrow;
        Vector3 lastPosition = frame[frame.Count-1];
        for (int i = 0; i < frame.Count; i++)
        {
            waytoDrow = lastPosition.WayPoints(frame[i],30);
            foreach (var point in waytoDrow)
            {
                DrawPoint dpoint = pull.Pulling();
                dpoint.transform.position = point;
                dpoint.ChangeColor(color);
            }

            lastPosition = frame[i];
        }
        
        ChoiceRandomDrawingPoint();

    }
    void ChoiceRandomDrawingPoint()
    {
        color = colors[Random.Range(0, colors.Count-1)];
    }
    public Shape RandomShape
    {
        get { return shapes[Random.Range(0,shapes.Count)]; }
    }

	// Update is called once per frame
	void Update () {
	
	}
}

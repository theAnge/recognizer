﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Extensions
{
    public static Vector3 Minus(this Vector3 vector, float num)
    {
        return new Vector3(vector.x - num, vector.y - num, vector.z - num);
    }
    public static List<Vector3> WayPoints(this Vector3 vector, Vector3 lastPoint, int numPoints)
    {
        List<Vector3> list = new List<Vector3>();
        list.Add(vector);
        for (int i = 1; i < numPoints; i++)
            list.Add(new Vector3(vector.x + ((lastPoint.x - vector.x) / numPoints) * i, vector.y + ((lastPoint.y - vector.y) / numPoints) * i));

        return list;
    }
    public static List<Vector3> FixFrame(this List<Vector3> frame)
    {
        float FixX = float.MaxValue, FixY = float.MaxValue;

        foreach (var position in frame)
        {
            if (position.x < FixX)
                FixX = position.x;
            if (position.y < FixY)
                FixY = position.y;

        }
        List<Vector3> fixFrame = new List<Vector3>();
        foreach (var position in frame)
        {
            fixFrame.Add(new Vector3(position.x - FixX, position.y - FixY));
        }

        return fixFrame;
    }
    public static List<Vector3> Clone(this List<Vector3> listToClone)
    {
        List<Vector3> cloneList = new List<Vector3>();
        foreach (var item in listToClone)
            cloneList.Add(item);
        return cloneList;
    }

    public static int[] IndexPrefAndNext(this List<Vector3> frame, int index)
    {
        int[] indexs = new int[3];
       
        if (index == 0)
        {
            indexs[0] = frame.Count - 1;
            indexs[1] = 0;
            indexs[2] = 1;
        }
        else
        {
            indexs[0] = index - 1;
            indexs[1] = index;
            indexs[2] = index + 1;
        }
        if (index == frame.Count - 1)
        {
            indexs[0] = frame.Count - 2;
            indexs[1] = frame.Count - 1;
            indexs[2] = 0;
        }
        return indexs;
    }
    public static int[] IndexListOutOfIndex(this List<Vector3> list, int index)
    {
        
        int[] indexs = new int[list.Count];
        int j = 0;
        for (int i = index; i < list.Count; i++,j++)
            indexs[j] = i;
        for (int i = 0; i != index; i++, j++)
            indexs[j] = i;
        return indexs;
    }
    public static void FalseAll(this bool[] target)
    {
        for (int i = 0; i < target.Length; i++)
        {
            target[i] = false;
        }
    }
    public static int numTrue(this bool[] target)
    {
        int i = 0;
        foreach (var item in target)
        {
            if (item == true)
                i++;
        }
        return i;
    }
    public static bool MaxExactlyNum(this List<int> list, int num)
    {
        int max = 0;
        foreach (var item in list)
            if (max < item) max = item;
        if (max < num)
            return false;
        else return true;

    }

}
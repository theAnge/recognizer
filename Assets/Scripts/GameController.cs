﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
    
public float TimeToFirstRound;
    public float DiverTimeToNextRound;
    float TimeRound;
    public static float TimeToEndRound = 0;
    public ShapeDrawer sDrower;
    public MouseWatcher mWatcher;
    Shape RoundShape;
    bool winRound = false;
    bool game = false;
    public GameObject button;
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
    public void StartGame()
    {
        Score.score = 0;
        StartCoroutine(Game());
        StartCoroutine(Drow());
    }
    IEnumerator Drow()
    {
        while (true)
        {
            if (game)
                sDrower.Drow(RoundShape);
            else
            {
                button.SetActive(true);
                yield break;
            }
            yield return new WaitForSeconds(1);
        }
    }
    IEnumerator Game()
    {
        game = true;
        TimeToEndRound = TimeToFirstRound;
        TimeRound = TimeToFirstRound;
        RoundShape = sDrower.RandomShape;
        sDrower.Drow(RoundShape);
        while (true)
        {
            yield return new WaitForSeconds(0);
            if (TimeToEndRound <= 0)
            {
                game = false;
                yield break;
            }
            if (winRound)
            {
                Score.score++;
                TimeToEndRound = TimeRound / DiverTimeToNextRound;
                TimeRound /= DiverTimeToNextRound;
                RoundShape = sDrower.RandomShape;
                sDrower.Drow(RoundShape);
                winRound = false;
                continue;
            }
            if (!mWatcher.Watch)
                winRound = SearchСonformityWithFrame();
            TimeToEndRound -= Time.deltaTime;
        }
    }
    const int MAX_ANGLE_DIFF = 25;
    const double MAX_ATTITUDE_DIFF = 0.30;
    const double MIN_PER_RECOGNITION = 70.0;

    bool SearchСonformityWithFrame()
    {
        var frame = this.RoundShape.frame;
        var UserShape = mWatcher.UserDrowShape;
        if (frame.Count > UserShape.Count)
            return false;
        bool[] check = new bool[frame.Count];
        List<int> listCheck = new List<int>();
        bool breaker= false;
        for (int i = 0; i < frame.Count; i++)
        {
            check.FalseAll();
            var indexsFrame = frame.IndexListOutOfIndex(i);
            int firstUserIndex = 0, midleUserIndex = 1, lastUserIndex = 2;
            for (int j = 0; j < indexsFrame.Length&&lastUserIndex<UserShape.Count;)
            {
                breaker = false;
                var indexsFrameTriangle = frame.IndexPrefAndNext(j);
                int angleFrame = countAngle(frame[indexsFrameTriangle[0]], frame[indexsFrameTriangle[1]], frame[indexsFrameTriangle[2]]);
                while( lastUserIndex < UserShape.Count)
                {
                    if (_abs(angleFrame - countAngle(UserShape[firstUserIndex], UserShape[midleUserIndex], UserShape[lastUserIndex])) <= MAX_ANGLE_DIFF&&lastUserIndex<UserShape.Count
                        /*&&_abs(countAngle(frame[indexsFrameTriangle[1]]+Vector3.right,frame[indexsFrameTriangle[1]], frame[indexsFrameTriangle[2]]) -countAngle(UserShape[midleUserIndex]+Vector3.right, UserShape[midleUserIndex], UserShape[lastUserIndex]))<30*/)
                    {
                        double attFrame = _abs(getDistance(frame[indexsFrameTriangle[0]], frame[indexsFrameTriangle[1]]) / getDistance(frame[indexsFrameTriangle[1]], frame[indexsFrameTriangle[2]]));
                        while (check[j] != true)
                        {
                            double attUser = (double)getDistance(UserShape[firstUserIndex], UserShape[midleUserIndex]) / getDistance(UserShape[midleUserIndex], UserShape[lastUserIndex]);
                            double att_diff = attUser - attFrame;
                            if (att_diff <= MAX_ATTITUDE_DIFF)
                            {
                                check[j] = true;
                                firstUserIndex = lastUserIndex;
                                midleUserIndex = firstUserIndex + 1;
                                lastUserIndex = midleUserIndex + 1;
                                breaker = true;
                                j++;
                                while (lastUserIndex < UserShape.Count)
                                {
                                    if (countAngle(UserShape[firstUserIndex], UserShape[midleUserIndex], UserShape[lastUserIndex]) >= 170)
                                        break;
                                    else
                                    {
                                        firstUserIndex++;
                                        midleUserIndex++;
                                        lastUserIndex++;
                                    }
                                }
                            }
                            else {
                                lastUserIndex++;
                                if (lastUserIndex == UserShape.Count) breaker = true;
                            }
                            if (breaker == true) break;
                        }
                    }
                    else
                    {
                        midleUserIndex++; lastUserIndex++; 
                    }
                    if (breaker == true) break;
                }
            }
            listCheck.Add(check.numTrue());
        }
        return listCheck.MaxExactlyNum(frame.Count-1);
    }

double getDistance(Vector3 p1, Vector3 p2)
{
    return System.Math.Sqrt(System.Math.Pow(p1.x-p2.x,2) + System.Math.Pow(p1.y-p2.y,2) );
}

double _abs( double dig)
{
    return (dig < 0) ? -dig : dig;
}

int _angle(double COS)
{
    double F_COS;
    int res = 0;
    int qu = (COS < 0) ? -1 : 1;
    for (int i = 0; i <= 90; i++)
    {
        F_COS = System.Math.Cos(i / 180.0 * System.Math.PI);
        if (F_COS - 0.01 <= qu * COS && F_COS + 0.01 >= qu * COS) { res = i; break; }
    }
    return (qu == -1) ? 180 - res : res;
}


int countAngle(Vector3 _a,Vector3 _b,Vector3 _c)
{
    double a = getDistance(_a, _b);
    double b = getDistance(_b, _c);
    double c = getDistance(_c, _a);

    double COS_ALPHA = (System.Math.Pow(a, 2) + System.Math.Pow(b, 2) - System.Math.Pow(c, 2)) / (2 * a * b);
    return _angle(COS_ALPHA);
}
}

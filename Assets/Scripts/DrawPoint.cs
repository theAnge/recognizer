﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class DrawPoint : MonoBehaviour
{
    public float LifeTime;
    public Pull pull;
    SpriteRenderer spriteRenderer;
    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = transform.localScale.Minus(Time.deltaTime / LifeTime);
        if (transform.localScale.x < 0.05f)
        {
            pull.Push(this);
        }
    }
    public void FixSize()
    {
        transform.localScale = Vector3.one;
    }
    public void ChangeColor(Color color)
    {
        spriteRenderer.material.SetColor("_Color",color);
    }
}
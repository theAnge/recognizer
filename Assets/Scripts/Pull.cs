﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pull : MonoBehaviour {
    List<DrawPoint> pull;
    // Use this for initialization
    public DrawPoint point;
    System.Random rand = new System.Random();
    void Start () {
        pull = new List<DrawPoint>();
	}
	

	// Update is called once per frame
	void Update () {
	
	}
    public void  Push(DrawPoint dpoint)
    {
        dpoint.FixSize();
        dpoint.gameObject.active = false;
        pull.Add(dpoint);
    }
    public DrawPoint Pulling()
    {
        if (pull.Count == 0)
        {
            DrawPoint dp = Instantiate(point);
            dp.pull = this;
            return dp;
        }
        else
        {
            DrawPoint point = pull[0];
            pull.Remove(point);
            point.gameObject.active = true;
            return point;
        }
    }
}
 
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    public Sprite[] numsSprite;
    public Image[] ScorNums;
    public static long score=0;
    long lastscore=-1;
    long diver = 10;
	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (score != lastscore)
        {
            long pointer = score;
            for (int i = 0; i < ScorNums.Length; i++)
            {
                ScorNums[i].sprite = numsSprite[pointer % diver];
                pointer /= diver;
            }
            lastscore = score;
        }
	}
}

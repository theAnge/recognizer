﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MouseWatcher : MonoBehaviour {
    List<Vector3> userDrowShape=new List<Vector3>();
    public List<Vector3> UserDrowShape
    {
        get { return userDrowShape; }
    }
    bool watch=true;
    public bool Watch {
        get {
            if (watch)
            {
                return watch;
            }
            else
            {
                watch = true;
                return false;
            }
        }
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
            StartCoroutine(Watching());
	}
    IEnumerator Watching()
    {
        this.userDrowShape.Clear();
        watch = true;
        while (true)
        {
            //Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            userDrowShape.Add(Input.mousePosition);
            if (Input.GetMouseButtonUp(0))
            {
                watch = false;
                yield break;
            }
            yield return new WaitForSeconds(0);

        }
    }
}
